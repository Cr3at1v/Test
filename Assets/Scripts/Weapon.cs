﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {
	public Transform bullet;
	public int BulletForce = 2500;
	public int Patron = 30;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
			if (Input.GetMouseButtonDown (0) & Patron > 0) {
			Transform BulletInstance = (Transform)Instantiate (bullet, GameObject.Find ("Spawn").transform.position, Quaternion.identity);
			BulletInstance.GetComponent<Rigidbody> ().AddForce (transform.forward * BulletForce);
			Patron = Patron - 1;
		}
		if (Input.GetKeyDown (KeyCode.R)) {
			Patron = 30;
		}
	}
}