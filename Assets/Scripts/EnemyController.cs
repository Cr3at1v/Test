﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour {
	public Transform chekpoint;
	NavMeshAgent agent;
	public Transform target; // переменая обьекта слежения
	public GameObject bullet;
	Transform Player;


	void Awake(){
//		Player = GameObject.FindWithTag ("Player").transform;
	}
	// Use this for initialization
	void Start () {
		agent = GetComponent<NavMeshAgent> ();
		Invoke("move", 5f);

	}

	void move(){
		CheakPoint point = chekpoint.GetComponent<CheakPoint> ();
		chekpoint = point.getNext ();
		agent.destination = chekpoint.position;
		Invoke("move", 5f);
	}

	// Update is called once per frame
	void Update () {
		transform.LookAt(target.transform.position); //поиск обьекта
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == "Player"){
			StartCoroutine("Shooting");
		}
	}

	void OnTriggerExit(Collider other){
		if(other.gameObject.tag == "Player"){
			StopCoroutine("Shooting");
		}
	}

	IEnumerator Shooting(){
		while (true) {
			Instantiate (bullet, target.position, target.rotation);
			yield return new WaitForSeconds (2);
		}
	}
}
