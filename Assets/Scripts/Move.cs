﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

	public GameObject player;
	public int speedRotation = 3;
	public int speed = 5;
	public int jumpSpeed = 50;

	float yAxis;
	float xAxis;

	int minY = -360;
	int maxY = 360;

	int minX = -0;
	int maxX = 0;

	float sensivity = 4;

	Vector3 rotationVector;


	void Start () { 
		player = (GameObject)this.gameObject; 
		}
	void Update ()
	{
		if (Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.UpArrow)) { 
			player.transform.position += player.transform.forward * speed * Time.deltaTime; 
		} 
		if (Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.DownArrow)) { 
			player.transform.position -= player.transform.forward * speed * Time.deltaTime; 
		} 
		if (Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.LeftArrow)) { 
			player.transform.position -= player.transform.right * speed * Time.deltaTime; 
		} 
		if (Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.RightArrow)) { 
			player.transform.position += player.transform.right * speed * Time.deltaTime; 
		} 
		{
		yAxis += Input.GetAxis ("Mouse X") * sensivity;
		xAxis = Mathf.Clamp (xAxis, minX, maxX);

		xAxis += Input.GetAxis ("Mouse Y") * sensivity;

		yAxis = Mathf.Clamp (yAxis, minY, maxY);

		rotationVector.Set (-xAxis, yAxis, 0);

	}

		transform.localEulerAngles = rotationVector;

	}
}
